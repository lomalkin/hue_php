#!/usr/bin/php
<?php

include_once "hue.inc.php";

if ($argc == 2) {
	if (strcasecmp($argv[1], "rand") == 0) {
		for ($i = 0; $i <= $c['count']; $i++) {
			hue_cmd($i, rand(1,255), rand(1,255), rand(1,65535));
		}
	}
	if (strcasecmp($argv[1], "on") == 0) {
		for ($i = 0; $i <= $c['count']; $i++) {
			hue_switch($i, true);
		}
	}
	if (strcasecmp($argv[1], "off") == 0) {
		for ($i = 0; $i <= $c['count']; $i++) {
			hue_switch($i, false);
		}
	}
	if (strcasecmp($argv[1], "link") == 0) {
		hue_link('{"devicetype":"hue_php#'.gethostname().'"}');
	}
} elseif ($argc == 3) {
	if (strcasecmp($argv[2], "rand") == 0) {
		hue_cmd($argv[1], rand(1,255), rand(1,255), rand(1,65535));
	} else {
		hue_switch($argv[1], (boolval($argv[2])) ? true : false);
	}
} elseif ($argc == 5) {
	hue_cmd($argv[1], $argv[2], $argv[3], $argv[4]);
} else {
	echo "Usage:";
	echo "\tAll: <on|off|rand|link>\n";
	echo "\t<lamp_num> <1|0|rand>\n";
	echo "\t<lamp_num> <sat> <bri> <hue>\n";
}

