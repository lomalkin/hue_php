# Philips HUE control PHP script

This stuff write in PHP language and should be used like console tool. But you can use it as you want.


# Requirements

* php-cli
* curl


# Configuration

You need copy or rename "config.php.example" to "config.php" and change IP and security key to yours.


# Compatibility

Tested in Ubuntu/Debian linux and works good.


# Contacts

Any questions and requests you should send to my email: [info@lomalkin.ru](mailto:info@lomalkin.ru?subject=hue_php)