<?php

include_once "config.php";

function hue_cmd($lampid, $sat, $bri, $hue) {
	$settings = '"on":true,"sat":'.intval($sat).',"bri":'.intval($bri).',"hue":'.intval($hue);
	hue_send($lampid, $settings);
} //

function hue_switch($lampid, $status) {
	$settings = ($status) ? '"on":true' : '"on":false';
	hue_send($lampid, $settings);
} //

function hue_send($lampid, $settings) {
	global $c;
	$cmd = "curl 'http://{$c['ip']}/api/{$c['key']}/lights/".intval($lampid)."/state' -X PUT --data-binary '{".$settings."}'";
	$cmd = $cmd . ' 2>/dev/null';

	ob_start();
	system($cmd);
	$res = ob_get_clean();
	
	$json = json_decode($res, 1);
	print_r($json);
} //

function hue_link($data) {
	global $c;
	echo "Data: $data\n";
	$cmd = "curl 'http://{$c['ip']}/api' -X POST --data-binary '".$data."'";
	$cmd = $cmd . ' 2>/dev/null';

	ob_start();
	system($cmd);
	$res = ob_get_clean();
	
	$json = json_decode($res, 1);
	print_r($json);
} //

